package com.small.nine.watch.dog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.small.nine.watch.dog"})
public class WatchDogApplication {

    public static void main(String[] args) {
        SpringApplication.run(WatchDogApplication.class, args);
    }

}
