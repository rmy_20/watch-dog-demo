package com.small.nine.watch.dog.service;

/**
 * 看门狗程序测试
 *
 * @author sheng_zs@126.com
 * @date 2021-07-26 9:22
 */
public interface WatchDogTestService {

    /**
     * 获取 access_token
     *
     * @return access_token
     */
    String getAccessToken();
}
