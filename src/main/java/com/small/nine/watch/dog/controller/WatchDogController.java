package com.small.nine.watch.dog.controller;

import com.small.nine.watch.dog.service.WatchDogTestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author sheng_zs@126.com
 * @date 2021-08-09 14:30
 */
@RestController
public class WatchDogController {
    @Resource
    private WatchDogTestService watchDogTestService;

    @GetMapping("/getToken")
    public String getToken() {
        return watchDogTestService.getAccessToken();
    }
}
